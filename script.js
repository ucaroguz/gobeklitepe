
// --- Code for modal ---  

//get modal
var modal = document.getElementById('myModal');

//get the button that opens the modal 
var button = document.getElementById('btn');

//get the span element that closes the modal
var span = document.getElementsByClassName('close')[0];

//When the user clicks the button,open the modal
btn.onclick = function() {
  modal.style.display = 'block';
}

//when the user clicks on span x, close the modal
span.onclick = function() {
  modal.style.display = 'none';
}

// when the user clicks outside of the modal, close it
window.onclick = function(e) {
  if(e.target === modal) {
    modal.style.display = 'none';
  }
}
